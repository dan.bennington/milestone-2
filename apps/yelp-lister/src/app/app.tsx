import { useState, useEffect } from 'react';
import {
  Grid,
  Typography
} from '@mui/material';
import {
  BusinessesList,
  SearchInput
} from '@react-v1/components';
import {
  yelpBusinessSearchData
} from '@react-v1/mock-data';
import {
  filterBusinesses,
  transformBusinesses
} from '@react-v1/utilities';
import {
  addFavorite,
  getFavorites,
  removeFavorite,
} from '@react-v1/data-access'
import {
  Business,
  Favorite
} from '@react-v1/types';

export function App() {
  const [ favorites, setFavorites ] = useState<Favorite[]>([])
  const [ filter, setFilter ] = useState<string | null>(null)
  const businesses = transformBusinesses(yelpBusinessSearchData.businesses)
  const filteredBusinesses = filterBusinesses(businesses, filter)

  const onSearch = (value: string | null) => {
    setFilter(value)
  }

  const addBusinessToFavorites = async (business: Business) => {
    setFavorites([
      ...favorites,
      { ...business, firebaseId: 'temp' }
    ])
    const addFavoriteResp = await addFavorite(business)
    if(addFavoriteResp instanceof Error) {
      setFavorites(favorites)
      return
    }
    const updatedFavorites = await getFavorites()
    if(updatedFavorites instanceof Error) return
    setFavorites(updatedFavorites)
  }

  const removeBusinessFromFavorites = async (firebaseId: string) => {
    setFavorites(
      favorites.filter(favorite => favorite.firebaseId !== firebaseId)
    )
    const removeFavoriteResp = await removeFavorite(firebaseId)
    if(removeFavoriteResp instanceof Error) {
      setFavorites(favorites)
      return
    }
    const updatedFavorites = await getFavorites()
    if(updatedFavorites instanceof Error) return
    setFavorites(updatedFavorites)
  }

  useEffect(() => {
    getFavorites().then(favoritesData => {
      if(favoritesData instanceof Error) return
      setFavorites(favoritesData)
    })
  }, [])
  
  return (
    <Grid>
      <Grid
        sx={{
          padding: '1rem 2rem 2.4rem'
        }}
      >
        <Typography
          sx={{
            textAlign: 'center',
            fontSize: '2rem',
            marginBottom: '1rem',
            fontWeight: '600',
          }}
        >
          Yelp Lister
        </Typography>
        <SearchInput onSearch={onSearch} />
      </Grid>
      <BusinessesList
        businesses={filteredBusinesses}
        favorites={favorites}
        addFavorite={addBusinessToFavorites}
        removeFavorite={removeBusinessFromFavorites} 
      />
    </Grid>
  );
}

export default App;
