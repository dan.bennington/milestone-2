export interface Business {
  id: string;
  name: string;
  isClosed: boolean;
  image: string;
  address: string[];
  phone: string;
  rating: number;
  categories: string[];
}

export interface Favorite extends Business {
  firebaseId: string;
}
