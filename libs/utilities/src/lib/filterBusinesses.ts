import { Business } from '@react-v1/types';

const splitAndSearch = (str: string, filter: string) => (
  str.split(' ').includes(filter)
)

const isCategoryMatch = (category: string, filter: string) => (
  splitAndSearch(category, filter) || category === filter
)

export const filterBusinesses = (businesses: Business[], filter: string | null) => {
  if(!filter) return businesses
  const lowerCaseFilter = filter.toLowerCase()
  return businesses.filter(({ name, categories }) => (
    categories.find(c => isCategoryMatch(c.toLowerCase(), lowerCaseFilter))
      || splitAndSearch(name.toLowerCase(), lowerCaseFilter)
  ))
}
